Affected images versions
========================
* not relevant (explain why)
* see the table below (list the **build id** and the **apt or ostree** deployment of the tested images in the appropriate cells)

| Type                    | Arch  | v2021 | v2022 | v2023dev2 |
| ----                    | ----  | ----- | ----- | --------- |
| minimal/fixedfunction   | amd64 |       |       |           |
| minimal/fixedfunction   | armhf |       |       |           |
| minimal/fixedfunction   | arm64 |       |       |           |
| target/hmi              | amd64 |       |       |           |
| target/hmi              | armhf |       |       |           |
| target/hmi              | arm64 |       |       |           |
| basesdk                 | amd64 |       |       |           |
| sdk                     | amd64 |       |       |           |
| nfs                     | amd64 |       |       |           |
| nfs                     | armhf |       |       |           |
| nfs                     | arm64 |       |       |           |
| lxc                     | amd64 |       |       |           |
| lxc                     | armhf |       |       |           |
| lxc                     | arm64 |       |       |           |
| image-builder           |       |       |       |           |
| package-source-builder  |       |       |       |           |

To find the build id and the variant type you can:
* derive it from the image name
  * for instance, with the `apertis_ostree_v2022pre-fixedfunction-amd64-uefi_20211031.0425.img.gz` image the build id is 20211031.0425, the variant is `fixedfunction` the deployment type is `ostree`
* obtain it from `/etc/os-release` using the `BUILD_ID` and `VARIANT_ID` keys

Unaffected images versions
==========================
* all versions up to XXX (replace XXX with the build id of the most recent Apertis images where this bug **cannot** be reproduced)   
* not relevant (explain why)
* v2020 (explain why)
* v2021 (explain why)
* v2022 (explain why)

Testcase
========
> The link to the testcase on https://qa.apertis.org/ if the bug was found during a testing round

Steps to reproduce
==================
> Ordered list detailing every step so other developers can reproduce the issue. Include hardware which you used.
* ...

Expected result
===============
> Explain (at least briefly) what result you expected.

Actual result
=============
> Explain the actual result observed after the execution steps.
> Paste any error output here between code block back-quotes.
> For long text contents (over 1000 lines) it is better to [create a new paste](https://phabricator.apertis.org/paste/edit/form/default/) and inline it here.

Reproducibility
===============
How often the issue is hit when repeating the test and changing nothing (same device, same image, etc.)?

Put the ✅ in the most appropriate entry:

1. always
2. ✅ often, but not always
3. rarely

Impact of bug
=============
> How severe is the bug? Does it render an image unbootable? Is it a security issue? Does it prevent specific applications from working?
> What is the impact? Does this bug affect a critical component? Does it cause something else to not work?
> How often is the bug likely to be found by a user? For example, every boot or once per year?

Attachments
===========
> Add further information about the environment in the form of attachments here.
> Attach plain text files from log output (from `journalctl`, `systemctl`, …) or long backtraces as inlined [paste objects](https://phabricator.apertis.org/paste/edit/form/default/).
> Screenshots and videos are usually useful for graphic issues.

Root cause
==========
> describe in one line what caused the issue to give a hint to product teams whether they may be impacted or not

Warranty
========
> Describe here the reason why the bug is #warranty, #not-warranty or still undecided

Outcomes
========
TBD

/label ~"status:unconfirmed" ~"priority:needs triage" ~"release:v2023dev2" ~"release:v2022" ~"release:v2021"

